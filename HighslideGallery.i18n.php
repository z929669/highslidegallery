<?php
/**
 * HighslideGallery extension
 *
 * Internationalisation file for extension HighslideGallery.
 *
 * @file
 * @ingroup Extensions
 * @author Brian McCloskey, Step Modifications
 * @copyright 2012 Brian McCloskey, 2020 Step Modifications
 * @license GPL-2.0-or-later
 */

$messages = array();

/** English
 * @author Brian McCloskey
 */
$messages['en'] = array(
    'hg-desc'  => "Allows creation of Highslide galleries on single images and Wiki Galleries.",
);

$magicWords = array();

$magicWords['en'] = array(
	'hsimg' => array(0, 'hsimg'),
);
